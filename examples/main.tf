terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.3"
}

provider "aws" {
  region = "us-east-1"
}

module "redirect_my_website" {
  # This isn't a standard module structure, but leaving as is because it's easy to copy and use
  source = "git::https://gitlab.com/crutt/terraform-s3-cloudfront-redirect.git//tf/modules/redirect_domain"

  tldn         = "mywebsite.com"
  redirect_url = "https://myotherwebsite.com"

  # Accepts a provider, if you have multiple defined
  providers = {
    aws = aws
  }
}