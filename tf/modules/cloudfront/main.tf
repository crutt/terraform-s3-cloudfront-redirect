terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

variable "tldn" {
  type        = string
  description = "The top level domain name (foo.com)"
}

variable "s3_redirect_website_endpoint" {
  type        = string
  description = "The website_endpoint of the s3 redirect bucket"
}

variable "cloudfront_access_identity_path" {
  type        = string
  description = "The cloudfront_access_identity_path of an OIA with GetObject permissions to the S3 bucket"
}

data "aws_route53_zone" "domain" {
  name         = var.tldn
  private_zone = false
}

resource "aws_route53_record" "domain" {
  name    = var.tldn
  zone_id = data.aws_route53_zone.domain.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.www_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.www_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www_domain" {
  name    = "www.${var.tldn}"
  zone_id = data.aws_route53_zone.domain.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.www_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.www_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_cloudfront_distribution" "www_distribution" {
  origin {
    /** 
     * This connects the S3 bucket created earlier to the Cloudfront 
     * distribution. 
     */
    origin_id   = "origin-${var.tldn}"
    domain_name = var.s3_redirect_website_endpoint

    # s3_origin_config {
    #   origin_access_identity = var.cloudfront_access_identity_path
    # }

    # https://docs.aws.amazon.com/AmazonCloudFront/latest/
    # DeveloperGuide/distribution-web-values-specify.html
    custom_origin_config {
      # "HTTP Only: CloudFront uses only HTTP to access the origin."
      # "Important: If your origin is an Amazon S3 bucket configured
      # as a website endpoint, you must choose this option. Amazon S3
      # doesn't support HTTPS connections for website endpoints."
      origin_protocol_policy = "http-only"

      http_port  = "80"
      https_port = "443"

      # TODO: is this needed?
      origin_ssl_protocols = ["TLSv1.2"]
    }
  }

  enabled = true

  aliases = ["www.${var.tldn}", var.tldn]

  price_class = "PriceClass_100"

  default_cache_behavior {
    target_origin_id       = "origin-${var.tldn}"
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    min_ttl                = 0
    default_ttl            = 300
    max_ttl                = 1200

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }


  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  /**
   * The AWS ACM Certificate is then applied to the distribution.
   */
  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.cert.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }
}