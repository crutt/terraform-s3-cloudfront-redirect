terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

variable "tldn" {
  type        = string
  description = "The top level domain name (foo.com)"
}

variable "redirect_url" {
  type        = string
  description = "The URL all bucket traffic will be directed to"
}

module "cloudfront" {
  source                          = "../cloudfront"
  tldn                            = var.tldn
  s3_redirect_website_endpoint    = module.s3_redirect_bucket.website.website_endpoint
  cloudfront_access_identity_path = module.s3_redirect_bucket.cloudfront_origin_access_identity.cloudfront_access_identity_path

  providers = {
    aws = aws
  }
}

module "s3_redirect_bucket" {
  source       = "../s3"
  tldn         = var.tldn
  redirect_url = var.redirect_url

  providers = {
    aws = aws
  }
}