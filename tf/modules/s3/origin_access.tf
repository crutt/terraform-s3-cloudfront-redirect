resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "access-identity-${var.tldn}.s3.amazonaws.com"
}

data "aws_iam_policy_document" "s3_bucket_policy" {
  statement {
    sid = "1"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${aws_s3_bucket.tldn_bucket.arn}/*",
    ]

    principals {
      type = "AWS"

      identifiers = [
        aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
      ]
    }
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = aws_s3_bucket.tldn_bucket.id
  policy = data.aws_iam_policy_document.s3_bucket_policy.json

}