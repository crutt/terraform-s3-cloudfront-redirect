terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

variable "tldn" {
  type        = string
  description = "The top level domain name (foo.com)"
}

variable "redirect_url" {
  type        = string
  description = "The URL all bucket traffic will be directed to"
}

# TLDN Bucket

resource "aws_s3_bucket" "tldn_bucket" {
  bucket = "redirect.${var.tldn}"
}

resource "aws_s3_bucket_acl" "tldn_acl" {
  bucket = aws_s3_bucket.tldn_bucket.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "tldn_redirect" {
  bucket = aws_s3_bucket.tldn_bucket.id

  routing_rule {
    redirect {
      host_name        = split("/", split("://", var.redirect_url)[1])[0]
      protocol         = split("://", var.redirect_url)[0]
      replace_key_with = (
        length(split("/", var.redirect_url)) > 3
        ? split("/", split("://", var.redirect_url)[1])[1]
        : ""
      )
    }
  }

  # Required when not using `redirect_all_requests_to`
  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

# Outputs
output "bucket" {
  value = aws_s3_bucket.tldn_bucket
}

output "website" {
  value = aws_s3_bucket_website_configuration.tldn_redirect
}

output "cloudfront_origin_access_identity" {
  value = aws_cloudfront_origin_access_identity.origin_access_identity
}